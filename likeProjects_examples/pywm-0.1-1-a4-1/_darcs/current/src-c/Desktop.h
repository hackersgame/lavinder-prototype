//@+leo-ver=4
//@+node:@file src-c/Desktop.h
//@@language c

// Desktop.H

extern void NextDesk();
extern void PreviousDesk();

class Desktop {
  const char* name_;
  int number_;
  static Desktop* current_;
public:
  static Desktop* first;
  Desktop* next;
  const char* name() const {return name_;}
  void name(const char*);
  int number() const {return number_;}
  static Desktop* current() {return current_;}
  static Desktop* number(int, int create = 0);
  static void current(Desktop*);
  static int available_number();
  static int max_number();
  Desktop(const char*, int);
  ~Desktop();
  int junk; // for temporary storage by menu builder
};

//@-node:@file src-c/Desktop.h
//@-leo
