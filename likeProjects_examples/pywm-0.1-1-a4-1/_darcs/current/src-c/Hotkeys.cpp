//@+leo-ver=4
//@+node:@file src-c/Hotkeys.cpp
//@@language c
// Hotkeys.C

// If you want to change what the hotkeys are, see the table at the bottom!

//@<< Hotkeys #includes >>
//@+node:<< Hotkeys #includes >>
#include <stdio.h>

#include "config.h"
#include "Frame.h"
#include "Desktop.h"

#include "pycallbacks.h"

//@-node:<< Hotkeys #includes >>
//@nl
//@<< Hotkeys declarations >>
//@+node:<< Hotkeys declarations >>

extern void ShowMenu();
extern void ShowTabMenu(int tab);
//@-node:<< Hotkeys declarations >>
//@nl
//@+others
//@+node:NextWindow

#if STANDARD_HOTKEYS

void NextWindow() { // Alt+Tab
  ShowTabMenu(1);
}
//@-node:NextWindow
//@+node:PreviousWindow
#endif

void PreviousWindow() { // Alt+Shift+Tab
  ShowTabMenu(-1);
}
//@-node:PreviousWindow
//@+node:DeskNumber

#if DESKTOPS

//warning: this assummes it is bound to Fn key:

static void DeskNumber() {
  Desktop::current(Desktop::number(Fl::event_key()-FL_F, 1));
}

#endif
//@-node:DeskNumber
//@+node:Raise

#if WMX_HOTKEYS || CDE_HOTKEYS

static void Raise() { // Alt+Up
  Frame* f = Frame::activeFrame();
  if (f) f->raise();
}
//@-node:Raise
//@+node:Lower

static void Lower() { // Alt+Down
  Frame* f = Frame::activeFrame();
  if (f) f->lower();
}
//@-node:Lower
//@+node:Iconize

static void Iconize() { // Alt+Enter
  Frame* f = Frame::activeFrame();
  if (f) f->iconize();
  else ShowMenu(); // so they can deiconize stuff
}
//@-node:Iconize
//@+node:Close

static void Close() { // Alt+Delete
  Frame* f = Frame::activeFrame();
  if (f) f->close();
}
//@-node:Close
//@+node:keybindings[]
////////////////////////////////////////////////////////////////

static struct {int key; void (*func)();} keybindings[] = {

#if STANDARD_HOTKEYS || MINIMUM_HOTKEYS
  // these are very common and tend not to conflict, due to Windoze:
  {FL_ALT+FL_Escape,	ShowMenu},
  {FL_ALT+FL_Menu,	ShowMenu},
#endif

#if STANDARD_HOTKEYS
  {FL_ALT+FL_Tab+FL_SHIFT,	NextWindow},
  {FL_ALT+FL_Tab,PreviousWindow},
  {FL_ALT+FL_SHIFT+0xfe20,PreviousWindow}, // XK_ISO_Left_Tab
#endif

#if KWM_HOTKEYS && DESKTOPS // KWM uses these to switch desktops
//   {FL_CTRL+FL_Tab,	NextDesk},
//   {FL_CTRL+FL_SHIFT+FL_Tab,PreviousDesk},
//   {FL_CTRL+FL_SHIFT+0xfe20,PreviousDesk}, // XK_ISO_Left_Tab
  {FL_CTRL+FL_F+1,	DeskNumber},
  {FL_CTRL+FL_F+2,	DeskNumber},
  {FL_CTRL+FL_F+3,	DeskNumber},
  {FL_CTRL+FL_F+4,	DeskNumber},
  {FL_CTRL+FL_F+5,	DeskNumber},
  {FL_CTRL+FL_F+6,	DeskNumber},
  {FL_CTRL+FL_F+7,	DeskNumber},
  {FL_CTRL+FL_F+8,	DeskNumber},
  {FL_CTRL+FL_F+9,	DeskNumber},
  {FL_CTRL+FL_F+10,	DeskNumber},
  {FL_CTRL+FL_F+11,	DeskNumber},
  {FL_CTRL+FL_F+12,	DeskNumber},
#endif

#if WMX_HOTKEYS
  // wmx also sets all these, they seem pretty useful:
  {FL_ALT+FL_SHIFT+FL_Up,	Raise},
  {FL_ALT+FL_SHIFT+FL_Down,	Lower},
  {FL_ALT+FL_SHIFT+FL_Enter,	Iconize},
  {FL_ALT+FL_SHIFT+FL_Delete,	Close},
  //{FL_ALT+FL_Page_Up,	ToggleMaxH},
  //{FL_ALT+FL_Page_Down,ToggleMaxW},
#endif

#if WMX_DESK_HOTKEYS && DESKTOPS
  // these wmx keys are not set by default as they break NetScape:
  {FL_ALT+FL_Left,	PreviousDesk},
  {FL_ALT+FL_Right,	NextDesk},
#endif

#if CDE_HOTKEYS
  // CDE hotkeys (or at least what SGI's 4DWM uses):
  {FL_ALT+FL_F+1,	Raise},
//{FL_ALT+FL_F+2,	unknown}, // KWM uses this to run a typed-in command
  {FL_ALT+FL_F+3,	Lower},
  {FL_ALT+FL_F+4,	Close}, // this matches KWM
//{FL_ALT+FL_F+5,	Restore}, // useless because no icons visible
//{FL_ALT+FL_F+6,	unknown}, // ?
//{FL_ALT+FL_F+7,	Move}, // grabs the window for movement
//{FL_ALT+FL_F+8,	Resize}, // grabs the window for resizing
  {FL_ALT+FL_F+9,	Iconize},
//{FL_ALT+FL_F+10,	Maximize},
//{FL_ALT+FL_F+11,	unknown}, // ?
  {FL_ALT+FL_F+12,	Close}, // actually does "quit"
#else
#if DESKTOPS && DESKTOP_HOTKEYS
  // seem to be common to Linux window managers
  {FL_ALT+FL_F+1,	DeskNumber},
  {FL_ALT+FL_F+2,	DeskNumber},
  {FL_ALT+FL_F+3,	DeskNumber},
  {FL_ALT+FL_F+4,	DeskNumber},
  {FL_ALT+FL_F+5,	DeskNumber},
  {FL_ALT+FL_F+6,	DeskNumber},
  {FL_ALT+FL_F+7,	DeskNumber},
  {FL_ALT+FL_F+8,	DeskNumber},
  {FL_ALT+FL_F+9,	DeskNumber},
  {FL_ALT+FL_F+10,	DeskNumber},
  {FL_ALT+FL_F+11,	DeskNumber},
  {FL_ALT+FL_F+12,	DeskNumber},
#endif
#endif
  {0}};

#endif
//@-node:keybindings[]
//@+node:Handle_Hotkey

int Handle_Hotkey()
{
  int key = Fl::event_key();

  //if (Fl::test_shortcut(FL_SHIFT + FL_ALT + key))
  //{
  //   printf("SHIFT-ALT %x\n");
  //   key += FL_SHIFT + FL_ALT;
  //}

  if (Fl::test_shortcut(FL_SHIFT + key))
    key |= FL_SHIFT + key;
  if (Fl::test_shortcut(FL_CTRL + key))
    key |= FL_CTRL + key;
  if (Fl::test_shortcut(FL_ALT + key))
    key |= FL_ALT + key;
  if (Fl::test_shortcut(FL_META + key))
    key |= FL_META + key;

  if (Fl::test_shortcut(FL_SHIFT + FL_CTRL + key))
    key |= FL_SHIFT + FL_CTRL + key;
  if (Fl::test_shortcut(FL_SHIFT + FL_ALT + key))
    key |= FL_SHIFT + FL_ALT + key;
  if (Fl::test_shortcut(FL_SHIFT + FL_META + key))
    key |= FL_SHIFT + FL_META + key;
  if (Fl::test_shortcut(FL_CTRL + FL_ALT + key))
    key |= FL_CTRL + FL_ALT + key;
  if (Fl::test_shortcut(FL_CTRL + FL_META + key))
    key |= FL_CTRL + FL_META + key;
  if (Fl::test_shortcut(FL_ALT + FL_META + key))
    key |= FL_ALT + FL_META + key;
  if (Fl::test_shortcut(FL_SHIFT + FL_CTRL + FL_ALT + key))
    key |= FL_SHIFT + FL_CTRL + FL_ALT + key;
  if (Fl::test_shortcut(FL_SHIFT + FL_CTRL + FL_META + key))
    key |= FL_SHIFT + FL_CTRL + FL_META + key;
  if (Fl::test_shortcut(FL_SHIFT + FL_ALT + FL_META + key))
    key |= FL_SHIFT + FL_ALT + FL_META + key;
  if (Fl::test_shortcut(FL_CTRL + FL_ALT + FL_META + key))
    key |= FL_CTRL + FL_ALT + FL_META + key;
  if (Fl::test_shortcut(FL_SHIFT + FL_CTRL + FL_ALT + FL_META + key))
    key |= FL_SHIFT + FL_CTRL + FL_ALT + FL_META + key;

  //printf("Handle_Hotkey: entered - key=%x\n", key);

  if (py_on_keyEvent(pyClient, (long)key))
      return 1;
                       
  for (int i = 0; keybindings[i].key; i++) {
    if (Fl::test_shortcut(keybindings[i].key)
        || ((keybindings[i].key & 0xFFFF) == FL_Delete
            	&& Fl::event_key() == FL_BackSpace// fltk bug?
             )
	)
    {
      keybindings[i].func();
      return 1;
    }
  }
  return 0;
}
//@-node:Handle_Hotkey
//@+node:Grab_Hotkeys

extern Fl_Window* Root;

void Grab_Hotkeys() {
  Window root = fl_xid(Root);
  for (int i = 0; keybindings[i].key; i++)
  {
    int k = keybindings[i].key;
    int keycode = XKeysymToKeycode(fl_display, k & 0xFFFF);
    if (!keycode)
        continue;
    // Silly X!  we need to ignore caps lock & numlock keys by grabbing
    // all the combinations:
    XGrabKey(fl_display, keycode, k>>16,     root, 0, 1, 1);
    XGrabKey(fl_display, keycode, (k>>16)|2, root, 0, 1, 1); // CapsLock
    XGrabKey(fl_display, keycode, (k>>16)|16, root, 0, 1, 1); // NumLock
    XGrabKey(fl_display, keycode, (k>>16)|18, root, 0, 1, 1); // both
  }
}
//@-node:Grab_Hotkeys
//@+node:Hotkeys_bindKey

void Hotkeys_bindKey(long key)
{
  Window root = fl_xid(Root);

  int keycode = XKeysymToKeycode(fl_display, key & 0xFFFF);
  if (!keycode)
     return;
 
  // Silly X!  we need to ignore caps lock & numlock keys by grabbing
  // all the combinations:
  XGrabKey(fl_display, keycode, key >>16, root, 0, 1, 1);
  //printf("Hotkeys:bindKey:ok - key=%x, keycode=%x\n", key, keycode);
}

//@-node:Hotkeys_bindKey
//@-others
//@-node:@file src-c/Hotkeys.cpp
//@-leo
