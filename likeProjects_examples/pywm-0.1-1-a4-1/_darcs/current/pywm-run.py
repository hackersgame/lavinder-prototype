#!/usr/bin/env python
#@+leo-ver=4
#@+node:@file pywm-run.py
#@@first
#@@language python

"""
pywm.py - Launcher utility for PYWM window manager applications.

Presents a menu offering a choice of window managers, and launches
the selected WM (or terminates).

Uses the environment variable PYWMDIR (default ~/.pywm) as the
location of user PYWM scripts to choose from.
"""

import sys, os, os.path, commands
from Tkinter import *

from pdb import set_trace as trace

class wmSelector:
    """
    Constructs and operates the window for WM script selection
    """
    scriptfiles = {}
    scriptnames = []
    
    def __init__(self):
        self.pywmDir = os.getenv("PYWMDIR")
        if not self.pywmDir:
            self.pywmDir = os.path.join(os.path.expanduser("~"), ".pywm")

        self.root = root = Tk()

        frm = self.frm = Frame(root)
        frm.pack(fill=BOTH, expand=1)
        
        self.scrW = root.winfo_screenwidth()
        self.scrH = root.winfo_screenheight()
        
        banner = Label(frm,
                       text="PYWM Selection Menu",
                       font="helvetica 16 bold")
        banner.pack(side=TOP, fill=X, expand=1)
                       
        heading = Label(frm, text="Please select a window manager")
        heading.pack(side=TOP, fill=X, expand=1)

        self.list = Listbox(frm, bg="white")
        self.list.pack(side=TOP, fill=BOTH, expand=1)
        
        buts = self.buts = Frame(frm)
        self.buts.pack(side=TOP, fill=X, expand=1)
        
        self.butOk = Button(buts,
                            text="OK",
                            width=10,
                            default=ACTIVE,
                            command=self.on_butOK)
        self.butOk.pack(side=RIGHT)
        
        self.butQuit = Button(buts,
                              text="Quit",
                              width=10,
                              command=self.on_butQuit)
        self.butQuit.pack(side=LEFT)

        # create user pywm dir if it doesn't yet exist
        if not os.path.isdir(self.pywmDir):
            self.createPywmDir()

        # hopefully, now there is a readable valid pywmDir
        self.readScripts()
        
    def readScripts(self):
        self.list.delete(0, END)
        files = os.listdir(self.pywmDir)
        for f in files:
            #print "f=%s" % f
            f1 = os.path.realpath(os.path.join(self.pywmDir, f))
            name, ext = os.path.splitext(f)
            #print "x?: f1=%s name=%s ext=%s" % (f1, name, ext)
            if os.path.isdir(f1):
                continue
            if ext in [".py", ".pyc", ".pyo"]:
                self.scriptnames.append(name)
                self.scriptfiles[name] = "python %s" % f1
                self.list.insert(END, name)
            elif ext == '':
                self.scriptnames.append(name)
                self.scriptfiles[name] = f1
                self.list.insert(END, name)

    def createPywmDir(self):
        """
        If a .pywm directory doesn't exist in user's home directory, create one
        and populate it with an example file and a README.
        """
        os.mkdir(self.pywmDir)
        import pywm.examples.example1 as example1
        ex1file = example1.__file__
        os.system("ln -s \"%s\" \"%s/%s\"" % (
             ex1file, self.pywmDir, os.path.split(ex1file)[1]))
        os.system("ln -s `which xterm` \"%s/X-Term\"" % self.pywmDir)
        #print "example1 file=", ex1file
        fd = file("%s/README.pywmdir" % self.pywmDir, "w")
        fd.write("""This directory contains the options for your PyWM launcher program.

Any files you place (or symlink) here bearing the extensions
'.py', '.pyc', or '.pyo' will be launched with the python interpreter.

Any files you place or symlink here with no extensions will be launched
with your default system shell.

Any files with extensions other than the above will be ignored.
""")
        fd.close()

        self.root.withdraw()
        self.top = Toplevel(self.root)
        self.top.title("PYWM Launcher Setup")
        frame = Frame(self.top)
        frame.pack(fill=BOTH, expand=1)
        Label(frame,
              wraplength=300,
              text="First time running PyWM launcher.\n\n"
                   +"Creating PyWM launcher directory:\n"
                   +"  "+self.pywmDir+"\n\n"
                   +"Refer to the README file in that directory for more information."
                   ).pack(fill=X, expand=1)
        Button(frame, text="OK", command=self.on_butOK_firstrun).pack(side=BOTTOM)

        
    def on_butOK_firstrun(self):
        self.root.deiconify()
        self.top.destroy()

    def on_butOK(self, event=None):
        sels = self.list.curselection()
        #print "ok: sels=", sels
        if len(sels) == 0:
            return
        #trace()
        selidx = int(sels[0])
        sel = self.scriptfiles[self.scriptnames[selidx]]
        #print "you selected file %s" % sel
        self.root.withdraw()
        print "want to run: %s" % sel

        #os.system(sel)
        logtext = commands.getoutput(sel)
        fd = file(os.path.join(self.pywmDir, "session.log"), "w")
        fd.write(logtext)
        fd.close()

        self.root.deiconify()
        self.root.tkraise()
    
    def on_butQuit(self, event=None):
        self.root.destroy()
        sys.exit(0)
        
    def run(self):
        g = self.root.geometry()
        #print "root geomeetry=%s" % g
        #print self.frm['width'], self.frm['height']
        #print "frm size is %dx%d" % (
        #   int(self.frm['width']),
        #   int(self.frm['height'])
        #   )
        w = 300
        h = 300
        x = (self.scrW - w) / 2
        y = (self.scrH - h) / 2
        #g = "%dx%d+%d+%d" % (w, h, x, y)
        #print "setting window geometry=%s" % g
        #self.root.geometry(g)
        self.root.geometry("+%d+%d" % (x, y))
        self.root.deiconify()
        self.root.tkraise()
        self.root.mainloop()

if __name__ == '__main__':
    wmSelector().run()



#@-node:@file pywm-run.py
#@-leo
