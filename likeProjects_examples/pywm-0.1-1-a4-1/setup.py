# -*- coding: utf-8 -*-
#@+leo-ver=4-thin
#@+node:davidmcnab.041604122248.6:@thin setup.py
#@@first

from distutils.core import setup
from distutils.extension import Extension
from Pyrex.Distutils import build_ext

import sys

# Obsolete, I think:
#pyIncludes = '/usr/include/python2.3'

libdirs = ['/usr/X11R6/lib'
           ]
defines = [
           ("FLTK_1_0_COMPAT", None) #Comment this if you use fltk1.0 instead of 1.1
           ]

# For dynamic linking
libs = [#'X11',
        #'Xext',
        'fltk',
        #'m'
        ]

# If you for some reason need to use static linking, this is where to specify the file
extra_link_args = [
                   #'/usr/lib/libfltk.a'
                   ]

# If you have your fltk headers somewhere special:
#fltkInclude = '../fltk-1.0.11'

include_dirs=[#fltkInclude,
              #pyIncludes,
              #"/usr/include",
              #"../src-c",
              #"../src-pyrex"
              ]

flwmSources=['src-pyrex/flwm_.pyx',
             'src-c/main.cpp',
             'src-c/Menu.cpp',
             'src-c/Hotkeys.cpp',
             'src-c/Rotated.cpp',
             'src-c/Desktop.cpp',
             'src-c/Frame.cpp',
             'src-c/FrameWindow.cpp',
             'src-c/flwmapi.cpp',
             ]

ext_flwm = [Extension('pywm.flwm_',
                      flwmSources,
                      include_dirs=include_dirs,
                      define_macros=defines,
                      libraries=libs,
                      library_dirs=libdirs,
                      extra_link_args=extra_link_args
                      )]
                      
setup(
  name = "pywm",
  packages = ["pywm", "pywm.examples"],
  package_dir = {'pywm':'src-python',
                 'pywm.examples':'examples'},
  version = '0.1',
  ext_modules=ext_flwm,
  cmdclass = {'build_ext': build_ext},
  scripts=["pywm-run.py"],
  
  maintainer="Elmo Mäntynen",
  maintainer_email="elmo13@jippii.fi",
  url="http://pywm.sourceforge.net/"
)
#@-node:davidmcnab.041604122248.6:@thin setup.py
#@-leo
