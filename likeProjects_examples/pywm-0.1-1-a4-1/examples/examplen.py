#!/usr/bin/env python
#@+leo-ver=4
#@+node:@file examples/examplen.py
#@@first
#@@language python
#@<< test declarations >>
#@+node:<< test declarations >>
#!/usr/bin/env python

import random
import pywm

#@-node:<< test declarations >>
#@nl
#@+others
#@+node:class myWindow
class myWindow(pywm.window):
    #@    @+others
    #@+node:myWindow.on_resize
    def on_resize(self, x, y, w, h):
        print "myWindow: %d %d %d %d %s" % (x, y, w, h, self.name)
    
    #@-node:myWindow.on_resize
    #@-others
#@-node:class myWindow
#@+node:class myWM
class myWM(pywm.WM):
    #@    @+others
    #@+node:myWM.__init__
    def __init__(self, **kwds):
    	pywm.WM.__init__(self, **kwds)
        #self.bindKey("z", self.hello)
        self.vi = None
    
    def hello(self):
        print "hello"
    
    #@-node:myWM.__init__
    #@+node:myWM.on_create
    def on_create(self, win):
    
        print "myWM.on_create: created window '%s'" % win.name
        #return
    
        x, y = win.position()
        #print "myWM:on_create: win.position succeeded"
        w, h = win.size()
        #print "myWM:on_create: win.size succeeded"
        #print "myWM:on_create: x=%d y=%d w=%d h=%d" % (x, y, w, h)
        #return
     
        # move the window
        if win.name == "xterm":
            #print "got xterm,, but bailing for now"
            #return
            self.xterm = win
            #print "myWM.on_create: trying to reposition and resize xterm"
            win.position(0, 0)
            win.size(600, 600)
            #print "myWM.on_create: repos/resize seems ok"
            return
            if self.vi:
                print "trying to close vi window"
                self.vi.close()
    
        if win.name == "vi":
            #print "myWM.on_create: got vi started"
            self.vi = win
            #return 
            self.launch("xterm")
            #print "myWM.on_create: tried to launch xterm"
    #@-node:myWM.on_create
    #@+node:myWM.on_destroy
    def on_destroy(self, win):
        print "myWM:on_destroy: destroyed window '%s'" % win.name
    #@-node:myWM.on_destroy
    #@+node:myWM.on_resize
    def on_resize(self, win, x, y, w, h):
        #print "on_resize"
        #print "myWM.on_resize: x=%d y=%d w=%d h=%d name=%s" % (x, y, w, h, win.name)
        return
    
        if win.name == 'Terminal':
            self.xterm.position(x, y-self.xterm.height())
        return 0
    
    #@-node:myWM.on_resize
    #@+node:myWM.on_buttonPress
    def on_buttonPress(self, button):
        print "myWM.on_buttonPress callback: button %d" % button
    #@-node:myWM.on_buttonPress
    #@+node:myWM.on_keyEvent
    def on_keyEvent(self, key):
        print "myWM.on_keyEvent callback: key=%d" % key
    
    #@-node:myWM.on_keyEvent
    #@+node:myWM.on_enter
    def on_enter(self, win):
        
        name = win.name
    
        x, y = self.getMousePosition()
        print "myWM.on_enter: x=%d y=%d" % (x, y)
        if name == 'vi':
            print "mouse at %d,%d" % (x, y)
            win.position(random.randint(0, 400), random.randint(0, 400))
    
    #@-node:myWM.on_enter
    #@+node:myWM.on_leave
    #def on_leave(self, hWin):
    #    
    #    win = self.windows[hWin]
    #    name = win.name
    #
    #    x, y = self.getMousePosition()
    #    print "on_leave: mouse at %d,%d" % (x, y)
    
    #@-node:myWM.on_leave
    #@-others
#@-node:class myWM
#@+node:run
def run():
    #wm = flwm.WM(fg="#ffcc00", bg="#000000", exit=1)
    wm = myWM(windowclass=myWindow,
              fg="#ffcc00",
              abg="#800080",
              bg="#000000",
              exit=1)
    wm.run()

#@-node:run
#@+node:MAINLINE
if __name__ == '__main__':
    run()
#@-node:MAINLINE
#@-others
#@-node:@file examples/examplen.py
#@-leo
