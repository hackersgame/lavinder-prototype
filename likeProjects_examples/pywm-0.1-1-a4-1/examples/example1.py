#!/usr/bin/env python
#@+leo-ver=4
#@+node:@file examples/example1.py
#@@first
#@@language python

"""
example1.y

Demonstrates an absolutely minimal window manager setup.
"""

import pywm

def run():
    windowManager = pywm.WM()
    windowManager.run()
    
if __name__ == '__main__':
    run()
#@-node:@file examples/example1.py
#@-leo
