#!/usr/bin/env python
#@+leo-ver=4
#@+node:@file examples/test.py
#@@first
#@@language python

import pywm

#@+others
#@+node:class window
class window(pywm.window):


    #@    @+others
    #@+node:__init__
    def __init__(self, hWin, wm, **kwds):
        pywm.window.__init__(self, hWin, wm, **kwds)
    #@nonl
    #@-node:__init__
    #@-others
#@nonl
#@-node:class window
#@+node:class WM
class WM(pywm.WM):
    def __init__(self):
        pywm.WM.__init__(
                         self,
                         windowclass=window,
                         exit=1
                         )
    #@    @+others
    #@+node:def on_startup
    def on_startup(self):
        """
        If you define an 'on_startup' method in your class, this
        method will get invoked just before the window manager
        engine enters its event loop. Note, however, that it
        gets called *after* the existing windows are discovered
        and the corresponding on_create callbacks fired.
        """
        print "myWindowManager: on_startup"
    
        # Bindings:
        self.bindKey(self.goNextDesk, None,
                     'right', 0, 1, 1, 0)
        self.bindKey(self.goPrevDesk, None,
                     'left', 0, 1, 1, 0)
        self.bindKey(self.launch, "xterm",
                     't', 0, 1, 1, 0)
    #@nonl
    #@-node:def on_startup
    #@+node:WM.on_buttonPress
    def on_buttonPress(self, button):
        """
        Called when a button click is received.
        Buttons 1, 2, 3, 4, 5 are left-click, middle-click, right-click,
        wheelscroll up and wheelscroll down, respectively.
    
        If you override this, you should accept a single argument,
        'button_num', and return 1 if you've handled it locally,
        or 0 if you want the WM to handle it instead.
    
        NOT YET WORKING
        """
        num_button={1:"left", 2:"middle", 3:"right", 4:"up", 5:"down"}
        print "wm.on_buttonPress callback: button %d, %s" % (button, num_button[button])
        if button == 1:
            print self.getMousePos()
        elif button == 2:
            pass
        elif button == 3:
            return 0
        elif button == 4:
            self.goNextWindow()
        elif button == 5:
            self.goPrevWindow()
        else:
            # disable menu except for right-click
                return 1
            return 1
    #@nonl
    #@-node:WM.on_buttonPress
    #@-others
#@nonl
#@-node:class WM
#@-others

def run():
    # create a window manager object using our class
    windowManager = WM()

    # and launch the window manager
    windowManager.run()

if __name__ == '__main__':
    run()
#@nonl
#@-node:@file examples/test.py
#@-leo
