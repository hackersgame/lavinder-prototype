#!/usr/bin/python3
import time

"""
This is what is used to auto indent
{<argumentLevel>: [<actual argument>, <suggestion>, <sentence>]}
put them in a list for multiple argument

"""

autoComp = ["FILE",
[{"cmd":  ["tell", "tell me about the file", "tell"]}] ,


  [
    {"arg1": ["m", "modified",  "last modified"]} 
  ],

  [
    {"arg1": ["s", "size", "me about the size"]}   ,
      {"arg2": ["g", "gigabytes", "in Gigabytes"]}  ,
      {"arg2": ["m", "MegaBytes", "in Megabytes"]}
  ]
]



def tell(fileName, args):
  if args[0] == "s":
    if args[-1] == "g":
      return ("du -k --block-size=1G " + fileName)
    elif args[-1] == "m":
      return ("du -km " + fileName)
    elif args[-1] == "d":
      return ("for i in 1 2 3 4 5; do echo $i;sleep 1; done")
    else:
      #default to gigabytes
      return ("du -k --block-size=1G " + fileName)

  if args[0] == "m":
    return ("date -r " + fileName)
