#!/usr/bin/python3
import time, os

"""
This is what is used to auto indent
{<argumentLevel>: [<actual argument>, <suggestion>, <sentence>]}
put them in a list for multiple argument

"""

#autoComp = ["FILE",
#[{"cmd":  ["rename", "rename file to", "rename"]}]]

autoComp = ["FILE",
[{"cmd":  ["rename", "rename file to", "rename "]}]]

def rename(fileName, args):
  if len(args) != 1:
    return("echo 'Error: Should just be one arg...'")
  newName = args[0]
  basePath = os.path.dirname(fileName)
  return("mv " + fileName + " " + basePath + "/" + newName)

