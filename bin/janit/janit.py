#!/usr/bin/python3
import curses,time,os,sys,threading, subprocess,tty, pty, re




#debug settings
global debugging
debugging = True

#prompt settings
global promptTxt
promptTxt = "ॐ "


#var used for data display
global outputData
global outputStart
global inputData
outputData = []
inputData = []

#prompt/color settings
#wiht black text
#8 = black
#7 = white
#6 = light blue (lblue)
#5 = purple
#4 = blue
#3 = tan
#2 = green
#1 = red
global colors
global promptColor
global completeColor

colors = {'red':1, 'green':2, 'tan':3, 'blue':4, 'purple':5, 'lblue':6, 'white':7, 'black':8}
promptColor = colors['white']
completeColor = colors['purple']
outputColor = colors['white']
menuColor = colors['blue']


#bash settings
runDir = "/tmp/"
master, slave = pty.openpty()
bashStdErr = runDir + "err_" + str(os.getpid())  + ".txt"
bashStdIN =  runDir + "in_" + str(os.getpid())  + ".txt"
bashStdOut =  runDir + "out_" + str(os.getpid())  + ".txt"
#make stdin file
#OSX issues... This call needs root on MAC... Lol
os.mknod(bashStdIN)

#modules for command 0_o
initModules = {}

desktopApplications = {}
applicationCategories = {}


menuShowing = False
#default menu: syntax {'MENUENTRY': ['categorie1', 'categorie2']}
mainMenuCategories = [{'Utility': ['Accessibility', 'System']}, {'Development': ['Development']}, {'Games': ['Games', 'Game']}, {'Graphics': ['Graphics']}, {'Internet': ['Network']}, {'Multimedia': ['AudioVideo']}, {'Office': ['Office']}, {'System': ['System', 'Settings']}, {'Other': ['Core', 'Settings', 'Screensaver']}]
menuFilter = ["Applications", ""]

#setup a list of categories based on the above
mainMenuCatList  = []
for tmp in mainMenuCategories:
  keyName = list(tmp.keys())[0]
  mainMenuCatList.append(keyName)

#create var that will hold tab options


#this will hold all autocomplete data (loaded in initCon)
autocompleteInfo = []

#this will hold autocomplete atm suggestions
autocomplete = ['testing', 'fishSticks', "time"]
  


#what we are running command for
global objectOfAttention
objectOfAttention = []

def startBash():
  command =  "/bin/bash > " + bashStdOut + " 2> " + bashStdErr
  subprocess.call(command, stdin=slave, shell=True)


#this will run and yield the output of bash and will display its output in the top blue box
def bashAttach():
  #run bash while piping it's ouput/err/in to new pts
  
  bashSubThread = threading.Thread(target=startBash)
  bashSubThread.start()
  
  #command =  "/bin/bash > " + bashStdOut + " 2> " + bashStdErr + ""
  #ran bash in background
  

  os.write(master, "ls -al\n".encode())
  
  tailCMD = "tail -f " + bashStdOut + " " + bashStdErr
  debug(tailCMD)
  #tailCMD="echo 1; sleep 1; echo 2"
  process = subprocess.Popen(tailCMD, stdout=subprocess.PIPE, shell=True)
  for data in process.stdout:
    yield data
    
    
    
    
  bashCMD = "bash"
  #tailCMD="echo 1; sleep 1; echo 2"
  bashProcess = subprocess.Popen(bashCMD, preexec_fn=os.setsid, stdin=slave, stdout=slave, stderr=slave, universal_newlines=True, shell=True)
  while bashProcess.poll() is None:
    data = os.read(master, 1026)
    yield data
    
#  for data in process.stdout:
#    yield data


  
def bashScreenRef(stdscr):
  for outputBit in bashAttach():
    #filter out all non ascii chars
    outputBit = str(outputBit).encode("ascii", errors="ignore").decode()
    
    #fix newlines and retruns
    outputBit = outputBit[2: -3]
    #outputBit = outputBit.replace('\\r', '\r')
    #if found it better to not have random \\r in output. :)
    outputBit = outputBit.replace('\\r', '')
    outputBit = outputBit.replace('\\t', '\t')
    

    #apped and update output
    outputData.append(outputBit)
    updateDisplay(std="out")


def bashRun(cmd):
  cmd = cmd + "\n"
  os.write(master, cmd.encode())
  #for byte in cmd + "\n":
    #os.write(master, bytes(byte, 'utf-8'))


def debug(error):
  global debugging
  
  if debugging:
    debugLog = open('./debug.txt', 'a+')
    debugLog.write(str(error) + "\n")
    debugLog.close()

        
      
def loadDesktopEntrys():
  
  #first find all the .desktops
  paths = ["/usr/share/applications/", "~/.local/share/applications/"]
  #paths = ["~/.local/share/applications/"]
  desktopEntrysFiles = []
  for path in paths:
    path = os.path.expanduser(path)
    walk = list(os.walk(path))

    for entry in walk[0][-1]:
      desktopEntrysFiles.append(path + entry)
      
  for fullPath in desktopEntrysFiles:
    #skip non .desktops
    if not fullPath.endswith(".desktop"):
      continue
    fh = open(fullPath)
    tmpEntry = {}
    for line in fh.readlines():
      #skip bad lines/first line
      if "=" in line:
        tmpEntry[line.split('=')[0]] = " ".join(line.split('=')[1:]).strip()
      
    if tmpEntry != {}:
      #check tmpEntry has what we need
      keys = list(tmpEntry.keys())
      if 'Type' in keys and 'Name' in keys and 'Exec' in keys:
        nameFixed = tmpEntry['Name'].replace("\xad", "")
        #check if this is a app
        if tmpEntry['Type'] == 'Application':
          #setup min info
          desktopApplications[nameFixed] = {'Exec': tmpEntry['Exec']}
          #add Categories if available
          if 'Categories' in keys:
            desktopApplications[nameFixed]['Categories'] = tmpEntry['Categories'].strip(";").split(";")
            #add new categories to known categories
            for cat in desktopApplications[nameFixed]['Categories']:
              if cat not in list(applicationCategories.keys()):
                applicationCategories[cat] = [nameFixed]
              else:
                if nameFixed not in applicationCategories[cat]:
                  applicationCategories[cat].append(nameFixed)
          #add command if available
          if 'Comment' in keys:
            desktopApplications[nameFixed]['Comment'] = tmpEntry['Comment']

  #we hand a list of all desktop entrys... Lets load them in mem
  debug(desktopApplications)
  debug(applicationCategories)
  #exit()

def initCon():
  global initModules
  scriptPath = os.path.dirname(os.path.realpath(__file__))
  
  loadDesktopEntrys()
  #setup initModules
  for brainFolder in os.walk(scriptPath + "/brain/"):
    #filter shit
    if brainFolder[0] == scriptPath + "/brain/" or brainFolder[0].endswith("/__pycache__"):
      continue
    
    #TODO this makes my eyes bleed... fix it
    #lol... builds somthing like: {'files': ['rename', 'tell'], 'always': ['switch']} by reading ./brain/*
    initModules[brainFolder[0].split("/")[-1]] =[i.split('/')[-1].split('.py')[0] for i in brainFolder[-1]]
    debug(initModules) 
  
  for modType in list(initModules.keys()):
    sys.path.insert(0, scriptPath + '/brain/' + modType + "/")
    for mod in initModules[modType]:
      importString = "import " + mod
      globalString = "global " + mod
      
      setupString = globalString + "\n" + importString
      #TODO
      #add try catch.. or find better way of importing
      #your mom shouldn't use exec :P
      exec(setupString)
      
      #getAutoComplete
      autocompleteString = "autocompleteInfo.append(" + mod + ".autoComp)"
      debug(autocompleteString)
      
      exec(autocompleteString)
      

def print(text, std='out', append=False):
  global outputData
  global outputStart
  global inputData

  if not append:
    if std == 'out':
      outputData.append(text)
    elif std == 'in':
      inputData.append(text)
  else:
    if std == 'out':
      outputData[-1] = outputData[-1] + text
    elif std == 'in':
      inputData[-1] = inputData[-1] + text
  updateDisplay(std=std)

    
    
def janitSay(whatToSay):
  termSize = stdscr.getmaxyx()
  
  #setup size
  janitSize = int(termSize[0]/2), int(termSize[1]/2)
  line = 1
  
  for lineText in whatToSay.split('\n'):

    stdscr.addstr(line, janitSize[1], lineText,curses.color_pair(outputColor))
    debug(lineText)
    line = line + 1


def updateDisplay(pop=False, clear=True, std='out', toggleMenu=False):
  global outputData
  global outputStart
  global inputData
  global menuShowing
  global menuFilter
  
  
  if toggleMenu:
    menuShowing = not menuShowing #menuShowing gets not menuShowing... okay... so menuShowing gets what? haha... :)
    
    
  termSize = stdscr.getmaxyx()
  yx = stdscr.getyx()

  if pop:
    inputData.pop(-1)
  
  
  
  if clear:
    #setup emptyLine
    emptyLine = ""
    for jojo in range(0, termSize[1]):
      emptyLine = emptyLine + " "
  
  
    #setup outputSpace
    for outputLine in range(0, outputStart):
      stdscr.move(outputLine, 0)
      stdscr.addstr(emptyLine,curses.color_pair(outputColor))
    
    
    
  printing = True
  textIndex = -1
  lineIndex = outputStart
  inputDataMaxIndex = lineIndex - len(inputData)
  while printing:
    #get line to print
    try:
      thisLine = (outputData + inputData)[textIndex]
      textIndex = textIndex -1
    except BaseException:
      printing = False
      break
    #split up line to fit 
    lineLines = []
    while thisLine:
      lineLines.append(thisLine[:termSize[1]])
      thisLine = thisLine[termSize[1]:]
    #we now have a line splitup to fit in the term
    newLineIndex = lineIndex - len(lineLines)

    for printIndex in range(lineIndex - len(lineLines), lineIndex):
      if printIndex < 0:
        continue
      #write new stuff and move back to wherever we were before.
      if printIndex >= inputDataMaxIndex:
        stdscr.addstr(printIndex, 0, lineLines[0],curses.color_pair(outputColor))
        stdscr.move(yx[0], yx[1])
        stdscr.refresh()
      else:
        stdscr.addstr(printIndex, 0, lineLines[0],curses.color_pair(outputColor))
        stdscr.move(yx[0], yx[1])
        stdscr.refresh()
      lineLines.pop(0)
    lineIndex = newLineIndex
  
  #display menu if needed
  if menuShowing:
    #setup menubackgound
    menuBase = ""
    for potato in range(0, int(termSize[1]/2)):
      menuBase = menuBase + " "
    
    #if ... show default Applications
    if menuFilter[0] == 'Applications':
      if menuFilter[1] == "":
        menuSize = len(mainMenuCategories)
        for greenPotato in range(outputStart - menuSize , outputStart):
          stdscr.move(greenPotato, 0)
          #draw base
          stdscr.addstr(menuBase,curses.color_pair(menuColor))
          #draw text
          stdscr.move(greenPotato, 0)
          adjustedIndex = abs((outputStart - menuSize) - greenPotato)
          debug(adjustedIndex)
          stdscr.addstr(list(mainMenuCategories[adjustedIndex].keys())[0],curses.color_pair(menuColor))
      elif menuFilter[1] in mainMenuCatList:
        categoriesToShow = []
        appList = []
        for tmpCat in mainMenuCategories:
          if list(tmpCat.keys())[0] == menuFilter[1]:
            debug("Yo: " + str(tmpCat[menuFilter[1]]))
            categoriesToShow = categoriesToShow + (tmpCat[menuFilter[1]])
            
        for knownCategorie in categoriesToShow: 
          if knownCategorie in applicationCategories:
            appList = appList + applicationCategories[knownCategorie]
        #appList "should" have a list of apps of type Application. and be of a categorie in categoriesToShow
        maxLen = outputStart
        placeHolder = ""
        for shitIDontNeed in menuFilter[1]:
          placeHolder = placeHolder + " "
        
        #setup size of menu
        if len(appList) >  maxLen:
          menuSize = maxLen
        else:
          menuSize = len(appList) 
        
        #find place to print categorie
        x = 0
        rootMenuLine = 0
        for findLineIndex in mainMenuCategories:
          if list(findLineIndex.keys())[0] == menuFilter[1]:
            rootMenuLine = x
            break
          x = x + 1
        rootMenuLine = len(mainMenuCategories) - rootMenuLine
        rootMenuLine = outputStart - rootMenuLine
        
        #find longest word
        longest = 0
        for menuItem in appList:
          if len(menuItem) > longest:
            longest = len(menuItem) 
        
        menuBase = ""
        minSize = len(menuFilter[1] + " ") + longest
        #don't shrink menu pash 1/2
        if int(termSize[1]/2) > minSize:
          minSize = int(termSize[1]/2)
          
        for i in range(0, len(menuFilter[1] + " ") + minSize):
          menuBase = menuBase + " "
        
        #menuFilter[1]
        drawMainMenu = ""
        adjustedIndex = 0
        for imAPotato in range(outputStart - menuSize , outputStart):
          stdscr.move(imAPotato, 0)
          #draw base
          stdscr.addstr(menuBase,curses.color_pair(menuColor))
          #draw text
          stdscr.move(imAPotato, 0)
          
          debug(adjustedIndex)

          
          if adjustedIndex == rootMenuLine:
            drawMainMenu = menuFilter[1]
          else:
            drawMainMenu = placeHolder
          stdscr.addstr(drawMainMenu + " " + appList[adjustedIndex],curses.color_pair(menuColor))
          adjustedIndex = adjustedIndex + 1
      elif menuFilter[1] in list(desktopApplications.keys()):
        menuSize = len(mainMenuCategories)
        for sourPotato in range(outputStart - menuSize , outputStart):
          stdscr.move(sourPotato, 0)
          #draw base
          stdscr.addstr(menuBase,curses.color_pair(menuColor))
          #draw text
          stdscr.move(sourPotato, 0)
          adjustedIndex = abs((outputStart - menuSize) - sourPotato)
          debug(adjustedIndex)
          stdscr.addstr("potato",curses.color_pair(menuColor))
#returns a list of autocomplete that work
def readCompleteGrid(completeGrids, commandText):
  debug("CompleteGridsLoaded: " + str(completeGrids))
  commandText = commandText.strip()
  suggestions = []
  fullText = ""
  #[{"cmd":  ["tell", "tell me about the file", ""]}] 
  #find agr level/tag(cmd,arg1,arg2,etc):
  levelTag = ""
  argLevel = len(commandText.split(" ")) -1
  splitCMD = commandText.split(" ")
  if argLevel == 0:
    levelTag = "cmd"
  elif argLevel > 0:
    argTag = "arg" + str(argLevel)
    
  #debug("_____________________" + commandText + "___________________")
  #loop the completeGrid group
  
  for completeGrid in completeGrids:
    for completeGroup in completeGrid:
      keepGoing = True
      rejectLevel = -1
      #check if this Group works
      for completeLine in completeGroup:

        #get tag and level of completeLine
        completeTag = list(completeLine.keys())[0]
        if completeTag == "cmd":
          completeLevel = 0
        else:
          completeLevel = int(completeTag.split("arg")[-1])
      
        command = completeLine[completeTag][0]
        if completeLevel -1  > argLevel:
          continue
        #should we even try?
        if rejectLevel != -1:
          if rejectLevel != completeLevel:
            continue
        

        if completeLevel > argLevel:
          #suggestions (the next thing you can enter)
          suggestions.append(list(completeLine.values())[0][-2])
          #debug("sug: " + str(completeLine))
        elif command == splitCMD[completeLevel]:
          #part is completely entered
          fullText = fullText + list(completeLine.values())[0][-1] + " "
          #debug("full: " + str(completeLine))
        elif command.startswith(splitCMD[completeLevel]):
          if splitCMD[completeLevel] == "":
            suggestions.append(list(completeLine.values())[0][-2])
          else:
            suggestions.append(list(completeLine.values())[0][-1])
          #debug("auto: " + str(completeLine))
          if list(completeLine.keys())[0] == "cmd":
            keepGoing = False
        else:
          #debug("reject: " + str(completeLine))
          rejectLevel = completeLevel
          #if cmd is rejected don't keep processing 
          if list(completeLine.keys())[0] == "cmd":
            keepGoing = False

      #check if we should keep going
      if not keepGoing:
        break

      
  #clean up fullText
  fullText = fullText.strip()
  #remove suggestions if need be
  if fullText == "":
    suggestions = suggestions[:1]
  debug([fullText, suggestions])
  return (fullText, suggestions)
    


#returns files/folders of a path... returns false if path cannot be found
def findFiles(path):
  options = []
  dirName = os.path.dirname(path)
  endPart = path.split(dirName)[-1].strip("/")
  if endPart != "":
    if os.path.isdir(path):
      dirName = path + "/"
  
  if not dirName.endswith("/"):
    dirName += "/"
  
  #if root dirname is not really a folder
  if not os.path.isdir(dirName):
    return ['wtf']
  
  
  #get list of what is in the root of the path
  for name in os.listdir(dirName):
    path = os.path.join(dirName, name)
    if os.path.isdir(path):
        name = name + "/"
    options.append(dirName + name)

  return options
  
  

def understand(someCMD):
  #no target yet.. lets check for some now and return
  if objectOfAttention == []:
    #find if this is a file
    if someCMD.startswith("/") or someCMD.startswith("./") or someCMD.startswith("~"):
      objectOfAttention.append(['FILE', someCMD])
    #IPV4
    if re.match("^(\d{0,3})\.(\d{0,3})\.(\d{0,3})\.(\d{0,3})$", someCMD):
      objectOfAttention.append(['IP', someCMD])      
    #we set a new target or we did not.. we don't want to continue.
    debug(objectOfAttention)
    return

  #run command from imported file
  splitCMD = someCMD.split(" ")
  functionName = splitCMD[0]
  args = splitCMD[1:]
  bashCode = ""

  #bad hack...
  fileTarget = objectOfAttention[-1][-1]
  debug("filename: " + fileTarget)
  bashCode = getattr(globals()[functionName], functionName)(fileTarget,  args)
  #exec(outputLoop)
  #print(bashCode)
  bashRun(bashCode)
  debug("BASHCODE: " + str(bashCode))
  return

  #deprecated
  try:
    exec(someCMD)
  except BaseException as e:
    debug(str(e))



def main():

  global outputStart
  global autocomplete
  global menuShowing
  global menuFilter
  
  termSize = stdscr.getmaxyx()
  TryAutocomplete = False
  MENU_CAN_RUN = ""

  
  #setup COLOR
  curses.start_color()
  curses.use_default_colors()
  #black text
  for i in range(0, curses.COLORS):
    #if color is light we want black text
    if i in [7,6,3]:
      curses.init_pair(i, 0, i);
    else:
      curses.init_pair(i, 7, i);
    #curses.init_pair(i, i, -1); #with default backgound


      
  inputline = termSize[0] - 1
  
  
  #setup outputSpace
  outputStart = termSize[0] - 4
  
  #setup colors n stuff
  emptyLine = ""
  for jojo in range(0, termSize[1]):
    emptyLine = emptyLine + " "
  
  #prompt color setup
  stdscr.move(inputline, 0)
  stdscr.addstr(emptyLine[-1],curses.color_pair(promptColor))
  
  #autocomplete color
  stdscr.move(inputline-1, 0)
  stdscr.addstr(emptyLine,curses.color_pair(completeColor))
  
  #setup outputSpace
  for outPutLine in range(0, outputStart):
    stdscr.move(outPutLine, 0)
    stdscr.addstr(emptyLine,curses.color_pair(outputColor))
  
  #setup input prompt
  stdscr.move(inputline, 0)
  stdscr.addstr(promptTxt,curses.color_pair(promptColor))
  stdscr.refresh()

  
  janitText = "i'm janit.\nI dO stuff\n"

  janitSay(janitText)


  #set input focus (bash or janit) 
  InputFocus = "janit"
  
  commandText = ""
  commandSentence = ""
  curses.noecho()
  promptIndex = 0
  canComplete = []
  inputOffset = 0
  level = 1
  #######TODO remove indent (this is a removed idea) TODO#######
  indent = 0
  #kick off shell thread (the top blue part)
  bashAbout = threading.Thread(target=bashScreenRef, args=(stdscr,))
  bashAbout.start()
  #done setting shit up. This is the main loop
  while True:
    debug('tick')
    # get keyboard input, returns -1 if none available
    c = stdscr.getkey()
    TryAutocomplete = False #only set to try when space is pressed

    #change input focus
    if c == 'KEY_BTAB':
      if InputFocus == "bash":
        InputFocus = "janit"
      elif(InputFocus == "janit"):
        InputFocus = "bash"
      continue


    ##############bash mode.. pipe shit to bash########################
    if InputFocus == "bash":
      os.write(master, c.encode())
      #TODO handle backspace
      if c not in ['\n', '\r', 'KEY_BACKSPACE']:
        print(c,append=True)
      
    #################################time for janit#########################
    if InputFocus == "janit":
      #run command on new line!! :)
      if c == "\n":
        #add new line to ouput if no text was entered
        if commandText.strip() == "":
          print(" ")
          continue
        if MENU_CAN_RUN:
          debug("running Menu cmd " + MENU_CAN_RUN)
          bashRun(MENU_CAN_RUN.split("%")[0])
        else:
          understand(commandText)
          debug("command: " + commandText)
        commandText = ""
        c = ""

          
      #catch tab (for autocompleteInfo)
      if c == "\t":
        if commandText == "":
          updateDisplay(toggleMenu=True)
          debug('menu toggled')
        TryAutocomplete = True
        c = ''
      #handle backspace
      if c == "KEY_BACKSPACE":
        deletedKey = ""
        #don't delete the prompt :)
        if commandText == "":
          continue
        if inputOffset == 0:
          deletedKey = commandText[-1]
          commandText = commandText[:-1]
        else:
          deletedKey = commandText[inputOffset -1]
          commandText = commandText[:inputOffset-1] + commandText[inputOffset:]
        
        #update dispaly on multi line input
        if deletedKey == "\n":
          updateDisplay(pop=True)
        #reset c so we don't add it to the command
        c = ""
      
      #handle left
      if c == "KEY_LEFT":
        #don't let you left key off the screen :)
        if abs(inputOffset) < len(commandText) and inputOffset <= 0:
          inputOffset = inputOffset - 1
        c = ""

      #handle key right   
      if c == "KEY_RIGHT":
        #only alow moving right if we have already moved left :)
        if inputOffset < 0:
          inputOffset = inputOffset + 1
        c = ""
        
      #find index to write this to the screen:
      promptIndex = len(promptTxt)

      #add command to commandText 
      if inputOffset == 0:
        commandText = commandText + c
      else:
        if c != "":
          commandText = commandText[:inputOffset] + c + commandText[inputOffset:]
      
      
      #rename ~ to real path
      if commandText == "~":
        commandText = os.path.expanduser("~") + "/"
        
        
      #check if we have a valid categorie in buffer
      knownCat = ""
      foundCommands = ""
      desktopApplications
      applicationCategories
      
      #check for menu commands in commandText
      for appName in list(desktopApplications.keys()):
        if appName in commandText:
          foundCommands = appName
          #TODO what if two commands match... I hope the last one was the needed one. :)
      #reset MENU_CAN_RUN if it was set before
      if not foundCommands and MENU_CAN_RUN:
        MENU_CAN_RUN = ""
        
      for superCat10000 in list(applicationCategories.keys()):
        if superCat10000 in commandText:
          knownCat = superCat10000
          
      
      if commandText.strip() in mainMenuCatList and foundCommands == "":
        debug("Found menu categorie")
        menuFilter = ["Applications", commandText.strip()]
        menuShowing = True
        updateDisplay()
      elif foundCommands:
        menuFilter = ["Applications", foundCommands]
        menuShowing = True
        MENU_CAN_RUN = desktopApplications[foundCommands]['Exec']
        debug("Found menu Command: " + MENU_CAN_RUN)
        updateDisplay()
      elif (menuShowing):
        menuFilter = ["Applications", ""]
        updateDisplay()
        
      #find what to autocomplete
      #if no target yet
      relevantCompleteGrid = []
      
      if len(objectOfAttention) == 0:
        if commandText.startswith("/") or commandText.startswith("./") or commandText.startswith("~"):
          #lets autocomplete a file Yo
          autocomplete = findFiles(commandText)
      #find autocompleGrids 
      else:
        typeName = objectOfAttention[-1][0]
        for completeGrid in autocompleteInfo:
          if completeGrid[0] == typeName:
            relevantCompleteGrid.append(completeGrid[1:])
        if len(relevantCompleteGrid) > 0:
          #get autocomplete data!!!!! :)
          commandSentence, autocomplete = readCompleteGrid(relevantCompleteGrid, commandText)
      """elif objectOfAttention[-1][0] == "FILE":
        #handle file types
        for completeGrid in autocompleteInfo:
          if completeGrid[0] == "FILE":
            relevantCompleteGrid.append(completeGrid[1:])
          if len(relevantCompleteGrid) > 0:
            #get autocomplete data!!!!! :)
            commandSentence, autocomplete = readCompleteGrid(relevantCompleteGrid, commandText)"""
      
      #find autocomplete options
      #reset stuff
      canComplete = []
      #find word we are entering atm
      lastWord = commandText.split(" ")[-1]
      
      #TODO add categories to auto complete
      autocomplete = autocomplete + mainMenuCatList
      
      #loop what we can autocomplete
      for possibility in autocomplete:
        if possibility.startswith(lastWord) and lastWord != "": 
          thisThing = [promptIndex + len(commandText.split("\n")[-1]) - len(lastWord), possibility]
          if thisThing not in canComplete:
            canComplete.append(thisThing)
        if commandSentence != "":
          if possibility.startswith(lastWord):
            canComplete.append([promptIndex + len(commandText.split("\n")[-1]) - len(lastWord), possibility])
          else:
            canComplete.append([promptIndex + len(commandText + " ".split("\n")[-1]), possibility])
      debug(canComplete)
      #do autocomplete
      if len(canComplete) == 1 and TryAutocomplete:
        text = canComplete[0][1]
        text = text.split(commandText.split("\n")[-1])[-1]
        debug("trying autocomplete")
        if inputOffset == 0:
          commandText = commandText + text
        else:
          if c != "":
            commandText = commandText[:inputOffset] + text + commandText[inputOffset:]
      
      
      #find len to end of prompt from command
      deadSpace = termSize[1] - promptIndex - len(commandText.split("\n")[-1]) - 1
      #create spaces to dispaly for empty space to the end of the prompt
      deadSpaceText = ""
      for yoyoGEEEE in range(0, deadSpace):
        deadSpaceText = deadSpaceText + " "

      
      #write autocomplete options :D
      #clear old autocompletes
      emptyLine = ""
      for jojo in range(0, termSize[1]):
        emptyLine = emptyLine + " "
      for catfish in range(1, 4):
        stdscr.move(inputline-catfish,0)
        stdscr.addstr(emptyLine,curses.color_pair(completeColor))
        
      #write new stuff
      level = 1
      maxLevel = 3
      for autocomp in canComplete:
        index = autocomp[0]
        text = autocomp[1]
        if text == "":
          debug("text in empty")
        debug("Text is '" + str(text) + "'")
        stdscr.move(inputline-level,index)
        stdscr.addstr(text,curses.color_pair(completeColor))
        level = level + 1
        if level > maxLevel:
          break
      
      
    
      #write sentence/update user input line
      if commandSentence != "":
        stdscr.addstr(inputline-3, 1, commandSentence,curses.color_pair(completeColor))
      
      #write command to prompt
      stdscr.move(inputline, promptIndex)
      stdscr.addstr(commandText.split("\n")[-1] + deadSpaceText,curses.color_pair(promptColor))
      #move prompt back to where it needs to be :)
      stdscr.move(inputline, promptIndex + len(commandText.split("\n")[-1]) + inputOffset)
      stdscr.refresh()
      




    


if __name__ == '__main__':
  global stdscr
  stdscr = curses.initscr()
  stdscr.keypad(1)
  
  #setupCommands
  initCon()
  
  main()
  try:
    main()
  except BaseException as e:
    debug(repr(e))
    curses.endwin()
  #curses.wrapper(main)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
